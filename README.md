# README

Repositório para materiais gráficos e textos usados no eventos organizados em
Curitiba como:

* FLISOL - Festival Latino-americano de Instalação de Software Livre
* SFD - Software Freedom Day
* MiniDebConf

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
