# SFD - Software Freedom Day

## O que é o Dia da Liberdade do Software?

O Software Freedom Day (SFD) ou em português, Dia da Liberdade do Software é uma celebração mundial do Software Livre e de Código Aberto (SL/CA). Nosso objetivo com essa festa é mostrar ao público em todo o mundo os benefícios de usar SL/CA de alta qualidade na educação, no governo, em casa, e nos negócios, ou seja, em todos os lugares! A organização sem fins lucrativos Digital Freedom Foudation (DFF) coordena o SFD em nível global, oferecendo apoio, brindes e um ponto de colaboração, mas times de voluntários em todo o mundo organizam os eventos SFD locais para atingir suas próprias comunidades.

## Muito obrigado aos nossos patrocinadores e apoiadores sem os quais não poderíamos fazer um evento como esse acontecer.

* Saiba mais sobre o Digital Freedom Foudation e SFD: http://www.digitalfreedomfoundation.org
* Leia sobre Como a liberdade do software sustenta os direitos humanos, da nossa ex-presidente, Pia Waugh: http://softwarefreedomday.org/content/software-freedom-underpinning-your-human-rights
* Também descubra mais sobre Software Livre: http://fsf.org/

## Visão

Nossa visão é a de capacitar todas as pessoas a conectar-se livremente, criar e compartilhar em um mundo digital que é participativo, transparente e sustentável.

## Objetivos

* Comemorar a liberdade do software e das pessoas em torno dela;
* Promover uma compreensão geral da liberdade do software e incentivar a adoção do software livre e de padrões abertos;
* Criar mais acesso igualitário de oportunidades através da utilização de tecnologias participativas;
* Promover o diálogo construtivo sobre responsabilidades e direitos na sociedade da informação;
* Trazer organizações e indivíduos que compartilham de nossa visão;
* Como organização, ser pragmático, transparente e responsável.

## Software Freedom Day em Curitiba:

http://sfd.curitibalivre.org.br

## Site internacional

http://softwarefreedomday.org

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
