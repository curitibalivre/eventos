# FLISOL 2014

Material gráfico baseado nos modelos criados por:

* María Leandro (Venezuela)
* Bárbara Tostes

Autor: Paulo Henrique de Lima Santana.

Arquivos SVG e PNG com vários tamanhos.

Veja outros modelos em:

http://www.flisol.info/FLISOL2014/MaterialGrafico
