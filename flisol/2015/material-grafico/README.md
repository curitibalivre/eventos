# FLISOL 2015

Material gráfico baseado nos modelos criados por:

* María Leandro (Venezuela)
* Bárbara Tostes
* Paulo Henrique de Lima Santana

Autor: Paulo Henrique de Lima Santana.

Arquivos SVG e PNG com vários tamanhos.

Veja outros modelos em:

http://www.flisol.info/FLISOL2015/MaterialGrafico
