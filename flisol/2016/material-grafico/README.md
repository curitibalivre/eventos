# FLISOL 2016

Material gráfico baseado nos modelos criados por:

* María Leandro (Venezuela)
* Bárbara Tostes
* Douglas de Lara dos Santos (banner-terminal)
* Paulo Henrique de Lima Santana

Autor: Paulo Henrique de Lima Santana.

Arquivos SVG, XCF e PNG com vários tamanhos.

Veja outros modelos em:

http://www.flisol.info/FLISOL2016/MaterialGrafico
